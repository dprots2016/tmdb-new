import { createStore } from "vuex";
import createPersistedState from 'vuex-persistedstate';
import { api } from "../app"

export default createStore({
    state: {
        page: 1,
        person: [],
        popularPersons: [],
        popularFilms: [],
        popularSerials: [],
        upcomingFilms: [],
        upcomingFilmsIds: [],
        trailersOfFilms: [],
        trendingMovies: [],
        favorites: [],
        favoriteMovies: [],
        movie: {}
    },
    getters: {
        GET_POPULAR_PERSONS: state => state.popularPersons,
        GET_POPULAR_FILMS: state => state.popularFilms,
        GET_POPULAR_SERIALS: state => state.popularSerials,
        GET_TRENDING_MOVIES: state => state.trendingMovies,
        GET_UPCOMING_FILMS: state => state.upcomingFilms,
        GET_TRAILERS_OF_FILMS: state => state.trailersOfFilms,
        GET_UPCOMING_FILMS_IDS: state => state.upcomingFilmsIds,
        GET_FAVORITES: state => state.favorites,
        GET_FAVORITE_MOVIES: state => state.favoriteMovies,
        GET_MOVIE: state => state.movie
    },
    actions: {
        ACT_GET_MOVIE({state, commit}, {type, id}) {
            api
                .get(`/${type}/${id}`)
                .then(res => {
                    commit('MUTATE', {
                        property: "movie",
                        value: res.data
                    });
                    api
                        .get(`/movie/${id}/credits`)
                        .then(res2 => {
                            commit('MUTATE', {
                                property: "movie",
                                property2: "credits",
                                value: res2.data
                            });
                        })
                })
                .catch(err => console.log(err))
        },
        GET_POPULAR_PERSONS_FROM_API({commit}, page) {
            api
                .get(`/person/popular`, {
                    params: {
                        "page": page || 1
                    }
                })
                .then(res => commit('MUTATE', {
                    property: 'popularPersons',
                    value: res.data
                }));
        },
        GET_PERSON_FROM_API({commit}) {
            api
                .get(`/person/popular`)
                .then(res => commit('SET_PERSON_TO_VUEX', res.data))
        },
        GET_POPULAR_FILMS_FROM_API({commit}, type) {
            api
                .get(`/${type}/popular`, {
                    params: {
                        "page": this.state.page
                    }
                })
                .then(res => {
                    res.data.results.forEach(item => item.media_type = type)
                    commit('MUTATE', {
                        property: "popularFilms",
                        value: res.data.results
                    });
                })
        },
        GET_TRENDING_MOVIES_FROM_API({commit}, time) {
            api
                .get(`/trending/all/${time}`)
                .then(res => {
                    commit('MUTATE', {
                        property: 'trendingMovies',
                        value: res.data
                    })
                })
                .catch(error => console.log(error))
        },
        GET_UPCOMING_FILMS_FROM_API({state, commit}) {
            api
                .get(`/movie/upcoming`, {
                    params: {
                        "page": this.state.page
                    }
                })
                .then(res => {
                    commit('MUTATE', {
                        property: 'upcomingFilms',
                        value: res.data
                    });
                    res.data.results.forEach((item) => {
                        if (!state.upcomingFilmsIds.includes(item.id)) {
                            state.upcomingFilmsIds.push(item.id);
                        }
                    })
                    state.upcomingFilmsIds.forEach(id => {
                        api
                            .get(`/movie/${id}/videos`)
                            .then(res => {
                                for (let i = 0; i < res.data.results.length; i++) {
                                    if (res.data.results[i]) {
                                        state.trailersOfFilms.push(res.data.results[i]);
                                        break;
                                    }
                                }
                            })
                    })
                })
        },
        ACT_CONCAT_POPULAR_SERIALS:({state, commit}) => {
            if (state.popularSerials.page) {
                commit('MUTATE',{
                    property: 'popularSerials.page',
                    value: state.popularSerials.page++
                })
            }
            api
                .get(`/tv/popular`, {
                    params: {
                        "page": state.popularSerials.page || 1
                    }
                })
                .then(res => {
                    res.data.results.forEach(item => item.media_type = 'tv')


                    if (state.popularSerials.results) {
                        commit('MUTATE',{
                            property: 'popularSerials',
                            property2: 'results',
                            value: state.popularSerials.results.concat(res.data.results)
                        })
                    } else {
                        commit('MUTATE',{
                            property: 'popularSerials',
                            value: res.data
                        })
                    }

                });

        },
        ACT_CONCAT_POPULAR_FILMS:({state, commit}) => {
            if (state.popularFilms.page) {
                commit('MUTATE',{
                    property: 'popularFilms',
                    property2: 'page',
                    value: state.popularFilms.page
                })
            }
            api
                .get(`/movie/popular`, {
                    params: {
                        page: state.popularFilms.page || 1
                    }
                })
                .then(res => {
                    res.data.results.forEach(el => el.media_type = 'movie')
                    if (state.popularFilms.results) {
                        commit('MUTATE', {
                            property: "popularFilms",
                            property2: 'results',
                            value: state.popularFilms.results.concat(res.data.results)
                        })
                    } else {
                        commit('MUTATE', {
                            property: "popularFilms",
                            value: res.data
                        })
                    }


                });
        },
        ACT_GET_FAVORITES_MOVIES:({commit, state}) => {
            let favoriteMovies = []
            Object.keys(state.favorites.movies).forEach(key => {
                api
                    .get(`/movie/${state.favorites.movies[key]}`)
                    .then(res => favoriteMovies.push(res.data));
            })
            commit('MUTATE', {
                property: 'favoriteMovies',
                value: favoriteMovies
            })

        },
        ACT_ADD_REMOVE_FAVORITES: ({state, commit}, card) => {
            if (state.favorites.some(el => el.id ===  card.id)) {
                commit('MUTATE', {
                    property: 'favorites',
                    value: state.favorites.filter(el => el.id !== card.id)
                })
            } else {
                commit('MUTATE', {
                    property: 'favorites',
                    value: [...state.favorites, card]
                })
            }

        },
        ACT_MUTATE: ({commit}, payload) => {
            commit('MUTATE', payload)
        }
    },
    mutations: {
        MUTATE: (state, payload) => {
            if (payload.property3) {
                state[payload.property][payload.property2][payload.property3] = payload.value
            } else if (payload.property2) {
                state[payload.property][payload.property2] = payload.value
            } else {
                state[payload.property] = payload.value
            }

        },
    }
    // plugins: [createPersistedState()],
})

