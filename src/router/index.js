import { createWebHistory } from 'vue-router';
import { createLangRouter } from 'vue-lang-router';

import translations from '../lang/translations';

import FilmsPage from '../pages/FilmsPage.vue'
import FilmsItemPage from '../pages/MoviePage.vue'
import HomePage from '../pages/HomePage.vue'
import SerialsPage from '../pages/SerialsPage.vue'
import PersonsPage from "../pages/PersonsPage.vue";
import PersonsItemPage from "../pages/PersonsItemPage.vue"
import SearchPage from "../pages/SearchPage.vue";
import FavoritesPage from "../pages/FavoritesPage.vue";
import NotFoundPage from "../pages/NotFoundPage.vue"

const routes = [
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: NotFoundPage
  },
  {
    path: '/',
    name: 'HomePage',
    component: HomePage,
    props: true
  },
  {
    path: '/:pathMatch(.*)*/films',
    name: 'FilmsPage',
    component: FilmsPage
  },
  {
    path: '/:pathMatch(.*)*/serials',
    name: 'SerialsPage',
    component: SerialsPage
  },
  {
    path: '/:pathMatch(.*)*/movie/:id',
    name: 'FilmsItemPage',
    component: FilmsItemPage,
    props: true,

  },
  {
    path: '/:pathMatch(.*)*/tv/:id',
    name: 'SerialsItemPage',
    component: FilmsItemPage,
    props: true,

  },
  {
    path: '/:pathMatch(.*)*/persons',
    name: 'PersonsPage',
    component: PersonsPage,
    props: true,

  },
  {
    path: '/:pathMatch(.*)*/person/:id',
    name: 'PersonsItemPage',
    component: PersonsItemPage,
    props: true,

  },
  {
    path: '/:pathMatch(.*)*/search',
    name: 'SearchPage',
    component: SearchPage,
    props: true,

  },
  {
    path: '/:pathMatch(.*)*/favorites',
    name: 'FavoritesPage',
    component: FavoritesPage,
    props: true,
  }
]

const langRouterOptions = {
  defaultLanguage: 'en',
  translations
};
const routerOptions = {
  routes,
  history: createWebHistory(),
};

const router = createLangRouter(langRouterOptions, routerOptions);

export default router;

