import {createApp} from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'
import App from './App.vue'
import router from './router'
import { Icon } from '@iconify/vue'
import Multiselect from 'vue-multiselect'
import { i18n } from 'vue-lang-router';

import './iconify/index.js';

import './assets/scss/build.scss'

export const app = createApp(App);

export const api = axios.create({
	baseURL: `${import.meta.env.VITE_BASE_URL}`,
	headers: {
		"Content-Type": "application/json",
	},
	params: {
		"api_key": "c64f302de27d71c7297f492d37e01c83"
	}
});

app.config.globalProperties.$axios = axios;
app.config.globalProperties.$api = api;

app.use(VueAxios, axios)
	.use(i18n)
	.use(router)
    .use(store)
	.component('Multiselect', Multiselect)
	.component('Icon', Icon)
    .mount("#app");
