export default {
    en: {
        name: 'English',
        load: () => { return import('./en.json'); },
    },
    ua: {
        name: 'Ukrainian',
        load: () => { return import('./ua.json'); },
    },
};
